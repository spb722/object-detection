
# object-detection flask app

object-detection with yolo model deployed using flask app

## Deployment

clone the project 

```bash
  https://gitlab.com/spb722/object-detection.git

```
go to the working directory of the project and install the requirement.txt 

```bash
  pip install -r requirement.txt

```

and to run the application 

```bash
  python webapp.py   

```
the application will be up and running in 

```bash
  http://127.0.0.1:5003

```





